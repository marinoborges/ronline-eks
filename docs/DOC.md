# EKS
https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_cluster
https://learn.hashicorp.com/tutorials/terraform/eks
https://github.com/terraform-aws-modules/terraform-aws-eks

# Workload
https://learn.hashicorp.com/tutorials/terraform/kubernetes-provider

# Backend
https://www.terraform.io/docs/backends/index.html

# Gitlab
https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html#configure-the-backend
https://gitlab.com/gitlab-org/configure/examples/gitlab-terraform-aws
https://gitlab.com/gitlab-org/terraform-images/
https://gitlab.com/gitlab-org/terraform-images/-/issues/18
