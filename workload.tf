variable "image" {
  type = string
  default = "nginx:1.7.8"
}

resource "kubernetes_deployment" "nginx" {
  metadata {
    name = "echoserver-nginx"
    labels = {
      App = "echoserver-nginx"
    }
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        App = "echoserver-nginx"
      }
    }
    template {
      metadata {
        labels = {
          App = "echoserver-nginx"
        }
      }
      spec {
        image_pull_secrets {
          name = kubernetes_secret.gitlab-registry-secret.metadata[0].name
        }
        container {
          image = "${var.image}"
          name  = "example"

          port {
            container_port = 8080
          }

          resources {
            limits {
              cpu    = "250m"
              memory = "64Mi"
            }
            requests {
              cpu    = "125m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "nginx" {
  metadata {
    name = "echoserver-nginx"
  }
  spec {
    selector = {
      App = kubernetes_deployment.nginx.spec.0.template.0.metadata[0].labels.App
    }
    port {
      port        = 80
      target_port = 8080
    }

    type = "LoadBalancer"
  }
}
