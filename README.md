# ROnline EKS
This repo provisions an AWS EKS cluster from scratch deploying a built from [Dockerfile](Dockerfile) echoserver (nginx) workload altogether.

## Used Technology
- Terraform
- Kubernetes
- Docker
- Nginx
- AWS
- GitLab CI

## Requisites
- AWS account with sufficient permissions
- Set Environment Variables: `TF_VAR_access_key` and `TF_VAR_secret_key`
- Configure access to Container Registry at [config.json file](config.json)

## Options to run it
- Locally: after setting the Environment Variables, run `terraform init` followed by `terraform plan` and `terraform apply`. In this case, the AWS profile must me configured locally with [AWS CLI](https://aws.amazon.com/cli/).
- GitLab CI: clone/fork the repo, set CI/CD Variables and run a pipeline whether manually or pushing to the repo

## How to check the deployment
If everything goes fine, at the end of `terraform apply`:
- an ELB address variable is shown preceded by string "lb_ip=". Use that URL in your browser in order to check the deployment.
- a KUBECONFIG variable is generated preceded by string "kubectl_config=". Use that KUBECONFIG to access the EKS cluster with `kubectl`

## Destroy
After running, it's possible to destroy all the infrastructure running `terraform destroy` (locally) or running the pipeline job `destroy` (GitLab CI)

## Delete terraform state lock (if necessary)
`curl -X DELETE --header "PRIVATE-TOKEN: XXXXXXXXX" https://gitlab.com/api/v4/projects/23332291/terraform/state/ronline-eks/lock`

## Get kubeconfig (after deployment)
`aws eks update-kubeconfig --name $(aws eks list-clusters | jq .clusters[] | sed 's/\"//g') --kubeconfig=~/.kube/config.$(aws eks list-clusters | jq .clusters[] | sed 's/\"//g').yaml`
