resource "kubernetes_secret" "gitlab-registry-secret" {
  metadata {
    name = "gitlab-registry-secret"
  }

  data = {
    ".dockerconfigjson" = "${file("${path.root}/config.json")}"
  }

  type = "kubernetes.io/dockerconfigjson"
}
